import { useEffect, useState } from "react";
import AppConfig from "../../AppConfig";
import { convertToQueryString } from "../utils";

const useResultadosDistritos = ({
  resultadoPor,
  resultadoListasPASO,
  eleccion,
  categoria,
  agrupacion,
  distrito,
}) => {
  const [isLoading, setLoading] = useState(true);
  const [resultados, setResultados] = useState<[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const myHeaders = new Headers();
        myHeaders.append("ngrok-skip-browser-warning", "1");
        const params = {
          eleccion: eleccion,
          categoria: categoria,
          agrupacion: agrupacion,
          distrito: distrito,
        };
        const res = await fetch(
          `${AppConfig.API_URL}/${
            resultadoListasPASO
              ? "resultadosDistritosSublistas?"
              : "resultadosDistritos?"
          }${convertToQueryString(params)}`,
          {
            headers: myHeaders,
          }
        );
        const json = await res.json();
        // @ts-ignore
        setResultados(Array.isArray(json) ? json : []);
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    };
    fetchData();
  }, [
    resultadoPor,
    resultadoListasPASO,
    eleccion,
    categoria,
    agrupacion,
    distrito,
  ]);

  return [resultados, isLoading];
};
export default useResultadosDistritos;
