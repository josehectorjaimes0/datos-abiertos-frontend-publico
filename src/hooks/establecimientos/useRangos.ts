import { useEffect, useState } from "react";
import AppConfig from "../../AppConfig";

const useRangos = () => {
  const [isLoading, setLoading] = useState(true);
  const [rangos, setRangos] = useState<[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const myHeaders = new Headers();
        myHeaders.append("ngrok-skip-browser-warning", "1");
        let url = `${AppConfig.API_URL}/rangos?base=d2022_establecimientos`;
        const res = await fetch(url, { headers: myHeaders });
        const json = await res.json();
        console.log(json);
        setRangos(json);
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    };
    fetchData();
  }, []);

  return { isLoadingRangos: isLoading, rangos };
};
export default useRangos;
