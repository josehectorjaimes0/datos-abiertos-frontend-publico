import { useEffect, useState } from "react";
import AppConfig from "../../AppConfig";

const useClae2 = () => {
  const [isLoading, setLoading] = useState(true);
  const [clae2, setClae2] = useState<[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const myHeaders = new Headers();
        myHeaders.append("ngrok-skip-browser-warning", "1");
        let url = `${AppConfig.API_URL}/clae2?base=d2022_establecimientos`;
        const res = await fetch(url, { headers: myHeaders });
        const json = await res.json();
        console.log(json);
        setClae2(json);
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    };
    fetchData();
  }, []);

  return { isLoadingClae2: isLoading, clae2 };
};
export default useClae2;
