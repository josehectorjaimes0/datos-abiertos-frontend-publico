import { useEffect, useState } from "react";
import AppConfig from "../../AppConfig";

const useProvincias = () => {
  const [isLoading, setLoading] = useState(true);
  const [provincias, setProvincias] = useState<[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const myHeaders = new Headers();
        myHeaders.append("ngrok-skip-browser-warning", "1");
        let url = `${AppConfig.API_URL}/provincias?base=d2022_establecimientos`;
        const res = await fetch(url, { headers: myHeaders });
        const json = await res.json();
        console.log(json);
        setProvincias(json);
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    };
    fetchData();
  }, []);

  return { isLoadingProvincias: isLoading, provincias };
};
export default useProvincias;
