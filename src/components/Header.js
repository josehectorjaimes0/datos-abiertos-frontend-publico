import { useEffect } from "react";
import { Table } from "antd";
import useResultados from "../../hooks/useResultados";

const Resultados = () => {
  const { resultados, isLoadingResultados } = useResultados();

  const columns = [
    { title: "Categoría", dataIndex: "nom_categoria", key: "nom_categoria" },
    { title: "Lista", dataIndex: "nom_agrupacion", key: "nom_agrupacion" },
    { title: "Votos", dataIndex: "votos", key: "votos" },
    { title: "Porcentaje", dataIndex: "porcentajes", key: "porcentajes" },
  ];

  useEffect(() => {
    console.log(`Cargar resultados`);
  });

  return (
    <Table
      loading={isLoadingResultados}
      dataSource={resultados}
      columns={columns}
    />
  );
};

export default Resultados;
